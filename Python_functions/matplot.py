#! /usr/bin/env python

import matplotlib.pyplot as plt 
import numpy as np
import math

t = np.linspace(0, 2 * math.pi, 400)

a = np.sin(t)
b = np.cos(t)
c = a + b

plt.plot(t, a, t, b, t, c)
plt.show()
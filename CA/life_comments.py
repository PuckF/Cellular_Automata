#! /usr/bin/env python

# a "#" starts a comment

"""
   You can write multiline comments with a tripple quote, either double or
   single quote should do the trick.
   Try not to put more then 80 characters on a single line (coding convention).
   Have a look at: https://www.python.org/dev/peps/pep-0008/
   If our code deviates from the rules above, follow the rules above. We are
   old farts with strange habits.
"""

# A function is called with "def (argument1, argument2, etc.):"
# You can have a function without arguments.
def life(people, gens):
    # Comment your code to make clear what or why you are doing something,
    # don't explain how you are doing it. Your code should be readable...

    # initialize the starting condition

    # create an empty list
    ca = []
    # variable assignment (not the same thing as in the line above)
    # variable with the name ca_length is assigned the value people
    ca_length = people

    # a for loop: for x in y
    # the loop iterates over all the values in y. The values are stored in x
    # x is called the iteration variable
    #
    # range is a function, range(start value, stop value, step value)
    # all values are integers. The start and step value are optional.
    # range returns a list of integers starting at 0 or start value till
    # stop value with step value as intervals e.g. range(3, 30, 3)
    for i in range(ca_length):
        # append is a "method" of the type list. It adds a value at the
        # end of the list. Other methods for lists are count, remove and sort
        ca.append(0)

    # if statement: if this is true do that or if expression: statements
    # can be combined with else or elif (else if)
    # the % is the modulo operator, it divides a number and returns the
    # remainder: 4 % 2 = 0 and 5 % 2 = 1
    # Make sure people is odd so that there is a nice symmetric print
    if people % 2 == 0:
        people += 1
    
    # people is an integer and when dividing by an integer the result
    # will be an integer
    middle_man = people / 2 + 1
    ca[middle_man] = 1

    # cells is a dictionary. A dictionary consists of 'keys' and 'values'
    # keys and values are seperated by a ':', different entries are seperated
    # by a ','. 
    cells = {0 : '-', 1 : '*'}
    # print first generation
    print ''.join(cells[e] for e in ca)

    # the above could be written as:
    """
        my_line = "" # empty string
        for e in ca:
            new_letter = cells[e] #look up the value in cells with e as the key
            my_line = my_line + new_letter # you can add strings together
        print my_line
    """

    generations = gens
    generation  = 1
    # a while loop, a conditional loop. With loops always make sure they 
    # "terminate" else your program will run forever
    # generation < generations is a comparison. A comparison returns True 
    # or False.
    while generation < generations:
        ca_new = []
        
        for j in range(ca_length):
            # "and", "or" and "not" are Boolean expressions 
            if (j > 0) and (j < ca_length - 1):
                # the == is not an assignment but compares two values/objects
                # if they are equal the comparison returns True, else False
                if (ca[j - 1] == ca[j + 1]):
                    ca_new.append(0)
                else:
                    ca_new.append(1)
            elif (j == 0):
                ca_new.append(0 if ca[1] == 0 else 1)
                # this could have been written as:
                """
                    if ca[1] == 0:
                       ca_new.append(0)
                    else:
                       ca_new.append(1)
                """
            elif (j == ca_length - 1):
                ca_new.append(0 if ca[j-1] == 0 else 1)

        print ''.join(cells[e] for e in ca_new)
        
        ca = ca_new[:]
        generation = generation + 1
   # end of while loop
# end of function

# beware of the indentation! 4 spaces or a tab 
# https://www.youtube.com/watch?v=SsoOG6ZeyUI

# variable assignment
people = 161
gens = 70
# calling the function life 
life(people, gens)

# this file was coded with vim and spaces...
